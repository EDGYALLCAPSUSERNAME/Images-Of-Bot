import time
import praw
import Users
import States
import Subreddits
import OAuth2Util


def search_for_states(r, o):
    submission_stream = praw.helpers.submission_stream(r, 'all')
    print("Searching for posts...")
    for submission in submission_stream:
        o.refresh()
        if submission.author in Users.users:
            continue
        if str(submission.subreddit).lower() in Subreddits.subreddits:
            continue
        if submission.over_18:  # skip if NSFW
            continue
        if 'imgur' not in submission.url and 'deviantart' not in submission.url:
            continue
        title = submission.title
        for state in States.states:
            if state in title.lower():
                make_post(r, submission, state)


def make_post(r, subm, state):
    title = "\"{}\" by /u/{} in /r/{}".format(subm.title, subm.author,
                                              subm.subreddit)
    comment = '[Original post]({}) in /r/{}'.format(subm.permalink,
                                                    subm.subreddit)
    state = state.title().replace(' ', '')
    subreddit = 'ImagesOf{}'.format(state)
    print("Making post in {}...".format(subreddit))
    while True:
        try:
            xpost = r.submit(subreddit, title, url=subm.url, captcha=None)
            xpost.add_comment(comment)
            break
        except praw.errors.AlreadySubmitted as e:
            print("Already submitted skipping...")
            break
        except praw.errors.RateLimitExceeded as e:
            print(("ERROR: Rate limit exceeded sleeping for " +
                   "{} seconds".format(e.sleep_time)))
            time.sleep(e.sleep_time)


def main():
    print("Logging in...")
    r = praw.Reddit('States_Of v1.0 /u/EDGYALLCAPSUSERNAME')
    o = OAuth2Util.OAuth2Util(r, print_log=True)

    while True:
        try:
            search_for_states(r, o)
        except praw.errors.HTTPException:
            print("Reddit is down. Sleeping...")
            time.sleep(360)

if __name__ == '__main__':
    main()
